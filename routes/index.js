const fetch = require("node-fetch");
export default function routes(app, addon) {
    // Redirect root path to /atlassian-connect.json,
    // which will be served by atlassian-connect-express.
    app.get('/', (req, res) => {
        res.redirect('/atlassian-connect.json');
    });
	
	app.get('/fbForm', addon.authenticate(), async (req, res) => {

     const current_page = req.query["pageId"];
	 const current_space = req.query["spaceKey"];
	 const page_Url = "https://jooble-docs.atlassian.net/wiki/spaces/" + current_space + "/pages/" + current_page;
     res.render('fbForm', {page_Url});
	});
}
